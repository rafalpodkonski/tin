package pl.s13302.backend.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.s13302.backend.model.Brand;
import pl.s13302.backend.model.Model;
import pl.s13302.backend.service.DictionaryService;

import java.util.List;

@Controller
@RequestMapping("/dictionary")
@CrossOrigin
@AllArgsConstructor
public class DictionaryController {

  private DictionaryService service;

  @RequestMapping(value = "/brands", method = RequestMethod.GET)
  public @ResponseBody List<Brand> readBrands() {
    return service.findAllBrands();
  }

  @RequestMapping(value = "/models", method = RequestMethod.GET)
  public @ResponseBody List<Model> readModels() {
    return service.findAllModels();
  }

  @RequestMapping(value = "/models/{brandId}")
  public @ResponseBody List<Model> readModels(@PathVariable("brandId") Long brandId) {
    return service.findAllModelsByBrandId(brandId);
  }

}
