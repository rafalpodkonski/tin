package pl.s13302.backend.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import pl.s13302.backend.model.Car;
import pl.s13302.backend.repository.CarRepository;
import pl.s13302.backend.service.CarService;

import java.util.Optional;

@Service
@AllArgsConstructor
public class CarServiceImpl implements CarService {

  private CarRepository repository;

  @Override
  public Page<Car> read(Specification<Car> spec, Pageable pageable) {
    return repository.findAll(spec, pageable);
  }

  @Override
  public Optional<Car> read(Long id) {
    return repository.findById(id);
  }

  @Override
  public Car create(Car car) {
    return repository.save(car);
  }
}
