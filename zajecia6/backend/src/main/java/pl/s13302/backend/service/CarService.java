package pl.s13302.backend.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import pl.s13302.backend.model.Car;

import java.util.Optional;

public interface CarService {

  Page<Car> read(Specification<Car> specification, Pageable pageable);

  Optional<Car> read(Long id);

//  List<Car> read(CarCriteria criteria);

  Car create(Car car);
}
