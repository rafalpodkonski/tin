package pl.s13302.backend.model.spec;

import org.springframework.data.jpa.domain.Specification;
import pl.s13302.backend.model.Car;

import javax.persistence.criteria.*;

public class CarWithProductionYear implements Specification<Car> {

  private final Integer productionYearBefore;
  private final Integer productionYearAfter;

  public CarWithProductionYear(Integer productionYearBefore, Integer productionYearAfter) {
    this.productionYearBefore = productionYearBefore;
    this.productionYearAfter = productionYearAfter;
  }

  @Override
  public Predicate toPredicate(Root<Car> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
    if (productionYearAfter == null && productionYearBefore == null) {
      return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
    }
    Path field = root.get("productionYear");
    if (productionYearAfter != null && productionYearBefore == null) {
      return criteriaBuilder.gt(field, productionYearAfter);
    }
    if (productionYearAfter == null && productionYearBefore != null) {
      return criteriaBuilder.le(field, productionYearBefore);
    }
    return criteriaBuilder.between(field, productionYearAfter, productionYearBefore);
  }
}
