package pl.s13302.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.s13302.backend.model.Brand;

public interface BrandRepository extends JpaRepository<Brand, Long> {
}
