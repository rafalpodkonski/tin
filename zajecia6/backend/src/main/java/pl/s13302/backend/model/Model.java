package pl.s13302.backend.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@Getter
public class Model {

  @Id
  private Long id;

  private String name;

  @ManyToOne(fetch = FetchType.EAGER)
  private Brand brand;

}
