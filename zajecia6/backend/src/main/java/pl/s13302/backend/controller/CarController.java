package pl.s13302.backend.controller;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import pl.s13302.backend.model.Car;
import pl.s13302.backend.model.spec.*;
import pl.s13302.backend.service.CarService;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

@Controller
@RequestMapping("/cars")
@CrossOrigin
@AllArgsConstructor
public class CarController {

  private CarService service;

  @GetMapping
  public @ResponseBody Page<Car> read(
    @RequestParam(value = "brand", required = false) Long brand,
    @RequestParam(value = "model", required = false) Long model,
    @RequestParam(value = "prodYearBefore", required = false) Integer productionYearBefore,
    @RequestParam(value = "prodYearAfter", required = false) Integer productionYearAfter,
    @RequestParam(value = "priceFrom", required = false) BigDecimal priceFrom,
    @RequestParam(value = "priceTo", required = false) BigDecimal priceTo,
    @RequestParam(value = "nextCarInspectionAfter", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date nextCarInspectionAfter,
    @RequestParam(value = "nextCarInspectionBefore", required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date nextCarInspectionBefore,
    Pageable pageable
  ) {
    Specification<Car> carSpecification = Specification
      .where(new CarWithBrand(brand))
      .and(new CarWithModel(model))
      .and(new CarWithProductionYear(productionYearBefore, productionYearAfter))
      .and(new CarWithPrice(priceFrom, priceTo))
      .and(new CarWithNextCarInspection(nextCarInspectionAfter, nextCarInspectionBefore));
    return service.read(carSpecification, pageable);
  }

  @GetMapping(value = "/{carId}")
  public @ResponseBody Optional<Car> read(@PathVariable Long carId) {
    return service.read(carId);
  }

  @PostMapping
  public @ResponseBody Car create(@RequestBody Car car) {
    return service.create(car);
  }
}
