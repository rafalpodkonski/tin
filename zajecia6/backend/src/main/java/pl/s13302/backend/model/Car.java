package pl.s13302.backend.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@NoArgsConstructor
@EqualsAndHashCode(exclude = {"id", "brand", "model", "productionYear", "nextCarInspection", "price"})
@ToString
@Getter
@Setter
@SequenceGenerator(name = "car_seq", initialValue = 1000, allocationSize = 100)
public class Car {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "car_seq")
  private Long id;

  @ManyToOne
  @JoinColumn(nullable = false)
  private Brand brand;

  @ManyToOne
  @JoinColumn(nullable = false)
  private Model model;

  @Column(nullable = false)
  private Integer productionYear;

  @Column(nullable = false)
  @JsonFormat(pattern = "yyyy-mm-dd")
  private Date nextCarInspection;

  @Column(nullable = false, scale = 2)
  private BigDecimal price;

}
