package pl.s13302.backend.model.spec;

import org.springframework.data.jpa.domain.Specification;
import pl.s13302.backend.model.Car;

import javax.persistence.criteria.*;
import java.util.Date;

public class CarWithNextCarInspection implements Specification<Car> {

  private final Date nextCarInspectionAfter;
  private final Date nextCarInspectionBefore;

  public CarWithNextCarInspection(Date nextCarInspectionAfter, Date nextCarInspectionBefore) {
    this.nextCarInspectionAfter = nextCarInspectionAfter;
    this.nextCarInspectionBefore = nextCarInspectionBefore;
  }

  @Override
  public Predicate toPredicate(Root<Car> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
    if (nextCarInspectionAfter == null && nextCarInspectionBefore == null) {
      return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
    }
    Path field = root.get("nextCarInspection");
    if (nextCarInspectionAfter != null && nextCarInspectionBefore == null) {
      return criteriaBuilder.greaterThan(field, nextCarInspectionAfter);
    }
    if (nextCarInspectionAfter == null && nextCarInspectionBefore != null) {
      return criteriaBuilder.lessThanOrEqualTo(field, nextCarInspectionBefore);
    }
    return criteriaBuilder.between(field, nextCarInspectionAfter, nextCarInspectionBefore);
  }
}
