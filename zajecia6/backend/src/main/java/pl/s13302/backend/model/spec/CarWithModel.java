package pl.s13302.backend.model.spec;

import org.springframework.data.jpa.domain.Specification;
import pl.s13302.backend.model.Car;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class CarWithModel implements Specification<Car> {

  private final Long modelId;

  public CarWithModel(Long modelId) {
    this.modelId = modelId;
  }

  @Override
  public Predicate toPredicate(Root<Car> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
    if (modelId == null) {
      return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
    }
    return criteriaBuilder.equal(root.get("model"), this.modelId);
  }

}
