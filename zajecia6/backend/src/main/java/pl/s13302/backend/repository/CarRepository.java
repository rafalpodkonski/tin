package pl.s13302.backend.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import pl.s13302.backend.model.Car;

public interface CarRepository extends JpaRepository<Car, Long> {

  Page<Car> findAll(Specification<Car> specification, Pageable pageable);

}
