package pl.s13302.backend.model.spec;

import org.springframework.data.jpa.domain.Specification;
import pl.s13302.backend.model.Car;

import javax.persistence.criteria.*;
import java.math.BigDecimal;

public class CarWithPrice implements Specification<Car> {

  private BigDecimal priceFrom;
  private BigDecimal priceTo;

  public CarWithPrice(BigDecimal priceFrom, BigDecimal priceTo) {
    this.priceFrom = priceFrom;
    this.priceTo = priceTo;
  }

  @Override
  public Predicate toPredicate(Root<Car> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
    if (priceFrom == null && priceTo == null) {
      return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
    }
    Path field = root.get("price");
    if (priceFrom != null && priceTo == null) {
      return criteriaBuilder.gt(field, priceFrom);
    }
    if (priceFrom == null && priceTo != null) {
      return criteriaBuilder.le(field, priceTo);
    }
    return criteriaBuilder.between(field, priceFrom, priceTo);
  }
}
