package pl.s13302.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.s13302.backend.model.Brand;
import pl.s13302.backend.model.Model;

import java.util.List;

public interface ModelRepository extends JpaRepository<Model, Long> {

  List<Model> findAllByBrand(Brand brand);

}
