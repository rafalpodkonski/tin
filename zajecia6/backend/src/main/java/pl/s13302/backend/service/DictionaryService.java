package pl.s13302.backend.service;

import pl.s13302.backend.model.Brand;
import pl.s13302.backend.model.Model;

import java.util.List;

public interface DictionaryService {

  List<Brand> findAllBrands();

  List<Model> findAllModels();

  List<Model> findAllModelsByBrandId(Long brandId);

}
