package pl.s13302.backend.model.spec;

import org.springframework.data.jpa.domain.Specification;
import pl.s13302.backend.model.Car;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class CarWithBrand implements Specification<Car> {

  private final Long brandId;

  public CarWithBrand(Long brandId) {
    this.brandId = brandId;
  }

  @Override
  public Predicate toPredicate(Root<Car> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
    if (brandId == null) {
      return criteriaBuilder.isTrue(criteriaBuilder.literal(true));
    }
    return criteriaBuilder.equal(root.get("brand"), brandId);
  }

}
