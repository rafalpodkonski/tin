package pl.s13302.backend.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import pl.s13302.backend.model.Brand;
import pl.s13302.backend.model.Model;
import pl.s13302.backend.repository.BrandRepository;
import pl.s13302.backend.repository.ModelRepository;
import pl.s13302.backend.service.DictionaryService;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class DictionaryServiceImpl implements DictionaryService {

  private ModelRepository modelRepository;
  private BrandRepository brandRepository;

  @Override
  public List<Brand> findAllBrands() {
    return brandRepository.findAll();
  }

  @Override
  public List<Model> findAllModels() {
    return modelRepository.findAll();
  }

  @Override
  public List<Model> findAllModelsByBrandId(Long brandId) {
    Optional<Brand> brandOptional = brandRepository.findById(brandId);
    return brandOptional.map(brand -> modelRepository.findAllByBrand(brand))
      .orElseThrow(() -> new IllegalArgumentException("Cannot find brand with id: " + brandId));
  }
}
