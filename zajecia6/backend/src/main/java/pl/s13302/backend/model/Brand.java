package pl.s13302.backend.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@NoArgsConstructor
@Getter
public class Brand {

  @Id
  private Long id;

  private String name;

  @OneToMany(mappedBy = "brand", fetch = FetchType.LAZY)
  @JsonIgnore
  private Set<Model> model;

}
