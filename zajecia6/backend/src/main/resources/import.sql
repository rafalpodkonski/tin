INSERT INTO `brand` (`id`, `name`) VALUES (1, 'BMW');
INSERT INTO `brand` (`id`, `name`) VALUES (2, 'Skoda');
INSERT INTO `brand` (`id`, `name`) VALUES (3, 'Audi');
INSERT INTO `brand` (`id`, `name`) VALUES (4, 'Volkswagen');

INSERT INTO `model` (`id`, `name`, `brand_id`) VALUES (1, 'i8', 1);
INSERT INTO `model` (`id`, `name`, `brand_id`) VALUES (2, 'i3', 1)
INSERT INTO `model` (`id`, `name`, `brand_id`) VALUES (3, 'CITIGOe iV', 2);
INSERT INTO `model` (`id`, `name`, `brand_id`) VALUES (4, 'e-tron', 3);
INSERT INTO `model` (`id`, `name`, `brand_id`) VALUES (5, 'ID.Buzz', 4);
INSERT INTO `model` (`id`, `name`, `brand_id`) VALUES (6, 'ID.Crozz', 4);

INSERT INTO `car` (`id`, `brand_id`, `model_id`, `production_year`, `next_car_inspection`, `price`) VALUES (1, 1, 1, 2019, '2019-12-31', 100.0);
INSERT INTO `car` (`id`, `brand_id`, `model_id`, `production_year`, `next_car_inspection`, `price`) VALUES (2, 2, 3, 2019, '2020-12-31', 150.0);
