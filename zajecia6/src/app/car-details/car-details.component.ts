import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { CarService } from '../services';
import { CarModel } from '../data';

@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.scss']
})
export class CarDetailsComponent implements OnInit {

  private carId: number;
  car: CarModel;
  error: any;
  loading = false;

  constructor(
    private route: ActivatedRoute,
    private service: CarService
  ) { }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.carId = params.carId;
      this.loadCarDetails();
    });
  }

  private loadCarDetails() {
    this.car = undefined;
    this.loading = true;
    this.service.readDetails(this.carId).subscribe(
      (car) => this.handleResponse(car),
      (error) => this.handleError(error),
      () => this.loading = false
    );
  }

  private handleResponse(car: CarModel) {
    this.car = car;
  }

  private handleError(error: any) {
    this.loading = false;
    this.error = error;
  }

}
