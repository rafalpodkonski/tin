import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { CarService, DictionaryService } from '../services';
import { Brand, CarCriteria, CarModel, Model, Page } from '../data';

@Component({
  selector: 'app-cars-list',
  templateUrl: './cars-list.component.html',
  styleUrls: ['./cars-list.component.scss']
})
export class CarsListComponent implements OnInit {

  cars: Page<CarModel> = undefined;
  brands: Array<Brand>;
  models: Array<Model>;
  error: Array<any> = new Array<any>();

  criteria = new CarCriteria();

  constructor(
    private carService: CarService,
    private dictionaryService: DictionaryService
  ) { }

  ngOnInit() {
    this.loadData();
  }

  private loadData(form: NgForm = null) {
    this.loadCars();
    this.loadBrands();
    this.loadModels();
  }

  private loadCars() {
    this.cars = undefined;
    this.carService.read(this.criteria).subscribe(
      (cars) => this.handleCarsResponse(cars),
      (error) => this.handleError(error)
    );
  }

  private loadBrands() {
    this.brands = undefined;
    this.dictionaryService.findAllBrands().subscribe(
      (value) => this.brands = value,
      (error) => this.handleError(error)
    );
  }

  private loadModels() {
    this.brands = undefined;
    this.dictionaryService.findAllModels().subscribe(
      (value) => this.models = value,
      (error) => this.handleError(error)
    );
  }

  private handleCarsResponse(cars: Page<CarModel>) {
    this.cars = cars;
  }

  private handleError(error: any) {
    console.error(error);
    this.error.push(error);
  }

}
