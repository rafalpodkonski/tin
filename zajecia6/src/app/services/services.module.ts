import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { CarService } from './car.service';
import { DictionaryService } from './dictionary.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    CarService,
    DictionaryService
  ]
})
export class ServicesModule { }
