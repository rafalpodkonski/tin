import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

import { Brand, Model } from '../data';

@Injectable({
  providedIn: 'root'
})
export class DictionaryService {

  private readonly apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  findAllBrands(): Observable<Array<Brand>> {
    return this.http.get<Array<Brand>>(`${this.apiUrl}dictionary/brands`);
  }

  findAllModels(): Observable<Array<Model>> {
    return this.http.get<Array<Model>>(`${this.apiUrl}dictionary/models`);
  }

  findAllBrandModels(brandId: number): Observable<Array<Model>> {
    return this.http.get<Array<Model>>(`${this.apiUrl}dictionary/models/${brandId}`);
  }

}
