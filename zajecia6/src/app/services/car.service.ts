import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

import { CarCriteria, CarModel, Page } from '../data';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  private readonly apiUrl = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Methods name like CRUD
   * C - create
   * R - read
   * U - update
   * D - delete
   */


  create(car: CarModel): Observable<CarModel> {
    return this.http.post<CarModel>(`${this.apiUrl}cars`, car);
  }

  read(carCriteria: CarCriteria): Observable<Page<CarModel>> {
    return this.http.get<Page<CarModel>>(`${this.apiUrl}cars${carCriteria.buildUrlParams()}`);
  }

  readDetails(carId: number): Observable<CarModel> {
    return this.http.get<CarModel>(`${this.apiUrl}cars\\${carId}`);
  }

}
