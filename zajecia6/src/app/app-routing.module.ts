import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CarsListComponent } from './cars-list/cars-list.component';
import { CarDetailsComponent } from './car-details/car-details.component';
import { CarFormComponent } from './car-form/car-form.component';

const routes: Routes = [{
  path: 'list',
  component: CarsListComponent
}, {
  path: 'details/:carId',
  component: CarDetailsComponent
}, {
  path: 'form',
  component: CarFormComponent
}, {
  path: '**',
  redirectTo: '/list',
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
