import { Component, OnInit } from '@angular/core';
import { FormGroup, NgModel } from '@angular/forms';
import { Router } from '@angular/router';

import { Brand, CarModel, Model } from '../data';
import { CarService, DictionaryService } from '../services';

@Component({
  selector: 'app-car-form',
  templateUrl: './car-form.component.html',
  styleUrls: ['./car-form.component.scss']
})
export class CarFormComponent implements OnInit {

  car: CarModel = new CarModel();
  error: any = undefined;
  success: boolean = undefined;

  brandsLoading = false;
  modelsLoading = false;

  brands: Array<Brand>;
  models: Array<Model>;

  constructor(
    private carService: CarService,
    private dictionaryService: DictionaryService,
    private router: Router
  ) { }

  ngOnInit() {
    this.loadBrands();
  }

  onBrandSelect(brandField: NgModel) {
    if (brandField.valid) {
      this.loadModels(brandField.value.id);
      this.car.model = undefined;
    }
  }

  onSubmit(form: FormGroup) {
    this.error = undefined;
    this.success = undefined;
    this.carService.create(this.car).subscribe(
      (car) => this.handleResponse(car),
      (error) => this.handleError(error)
    );
  }

  private loadBrands() {
    this.brandsLoading = true;
    this.dictionaryService.findAllBrands().subscribe(
      value => this.brands = value,
      error => console.error(error),
      () => this.brandsLoading = false
    );
  }

  private loadModels(brandId: number) {
    this.modelsLoading = true;
    this.dictionaryService.findAllBrandModels(brandId).subscribe(
      value => this.models = value,
      error => console.error(error),
      () => this.modelsLoading = false
    );
  }

  private handleResponse(car: CarModel) {
    this.car = car;
    this.success = true;
    this.router.navigate(['list']);
  }

  private handleError(error: any) {
    this.error = error;
    this.success = false;
  }

}
