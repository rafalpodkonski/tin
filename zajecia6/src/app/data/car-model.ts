import { Brand, Model } from '.';

export class CarModel {
  brand: Brand;
  model: Model;
  productionYear: number;
  nextCarInspection: Date;
  price: number;
}
