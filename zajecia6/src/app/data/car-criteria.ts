import { isNullOrUndefined } from 'util';

export class CarCriteria {

  private static readonly BRAND_PARAM = '&brand';
  private static readonly MODEL_PARAM = '&model';
  private static readonly PRODUCTION_YEAR_AFTER_PARAM = '&prodYearAfter';
  private static readonly PRODUCTION_YEAR_BEFORE_PARAM = '&prodYearBefore';
  private static readonly NEXT_CAR_INSPECTION_AFTER_PARAM = '&nextCarInspectionAfter';
  private static readonly NEXT_CAR_INSPECTION_BEFORE_PARAM = '&nextCarInspectionBefore';
  private static readonly PRICE_FROM_PARAM = '&priceFrom';
  private static readonly PRICE_TO_PARAM = '&priceTo';

  brand: string;
  model: string;
  prodYearAfter: number;
  prodYearBefore: number;
  nextCarInspectionAfter: string;
  nextCarInspectionBefore: string;
  priceFrom: number;
  priceTo: number;

  buildUrlParams(): string {
    let result = '?';
    if (this.isBrand()) {
      result += `${CarCriteria.BRAND_PARAM}=${this.brand}`;
    }
    if (this.isModel()) {
      result += `${CarCriteria.MODEL_PARAM}=${this.model}`;
    }
    if (this.isProdYearAfter()) {
      result += `${CarCriteria.PRODUCTION_YEAR_AFTER_PARAM}=${this.prodYearAfter}`;
    }
    if (this.isProdYearBefore()) {
      result += `${CarCriteria.PRODUCTION_YEAR_BEFORE_PARAM}=${this.prodYearBefore}`;
    }
    if (this.isNextCarInspectionAfter()) {
      result += `${CarCriteria.NEXT_CAR_INSPECTION_AFTER_PARAM}=${this.nextCarInspectionAfter}`;
    }
    if (this.isNextCarInspectionBefore()) {
      result += `${CarCriteria.NEXT_CAR_INSPECTION_BEFORE_PARAM}=${this.nextCarInspectionBefore}`;
    }
    if (this.isPriceFrom()) {
      result += `${CarCriteria.PRICE_FROM_PARAM}=${this.priceFrom}`;
    }
    if (this.isPriceTo()) {
      result += `${CarCriteria.PRICE_TO_PARAM}=${this.priceTo}`;
    }
    return result;
  }

  private isBrand(): boolean {
    return !isNullOrUndefined(this.brand);
  }

  private isModel(): boolean {
    return !isNullOrUndefined(this.model);
  }

  private isProdYearAfter(): boolean {
    return !isNullOrUndefined(this.prodYearAfter);
  }

  private isProdYearBefore(): boolean {
    return !isNullOrUndefined(this.prodYearBefore);
  }

  private isNextCarInspectionAfter(): boolean {
    return !isNullOrUndefined(this.nextCarInspectionAfter);
  }

  private isNextCarInspectionBefore(): boolean {
    return !isNullOrUndefined(this.nextCarInspectionBefore);
  }

  private isPriceFrom(): boolean {
    return !isNullOrUndefined(this.priceFrom);
  }

  private isPriceTo(): boolean {
    return !isNullOrUndefined(this.priceTo);
  }

}
