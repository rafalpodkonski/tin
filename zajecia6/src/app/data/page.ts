export class Page<T> {

  content: Array<T>;
  pageable: any;
  totalPages: number;
  last: boolean;
  totalElements: number;
  size: number;
  sort: any;
  first: boolean;
  numberOfElements: number;
  empty: number;

}
