import { Brand } from '.';

export class Model {
  id: number;
  name: string;
  brand: Brand;
}
