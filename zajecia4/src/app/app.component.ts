import { Component, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  clickEventEmitter = new EventEmitter<string>();
  cityName = 'Warszawa';

  submitForm(form) {
    this.cityName = (form.form.value.city);
    this.clickEventEmitter.emit(this.cityName);
  }

}
