import { Component, OnInit, Input, EventEmitter } from '@angular/core';
import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.sass']
})
export class WeatherComponent implements OnInit {

  @Input() clickEvent: EventEmitter<string>;
  loading = false;
  weather: any;
  error: any;

  constructor(
    private weatherService: WeatherService
  ) { }

  ngOnInit() {
    this.clickEvent.subscribe((city) => this.pullNewWeatherInfo(city));
  }

  private pullNewWeatherInfo(city: string) {
    this.loading = true;
    this.weatherService.getCurrentWeather(city)
      .subscribe(
        response => this.handleResponse(response),
        error => this.handleError(error)
      );
  }

  private handleResponse(response) {
    this.loading = false;
    this.error = undefined;
    this.weather = response;
    this.weather.weather.icon = `http://openweathermap.org/img/wn/${this.weather.weather[0].icon}@2x.png`;
  }

  private handleError(error) {
    this.loading = false;
    this.weather = undefined;
    this.error = error;
  }

}
