import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private readonly apiKey: string = environment.weatherApiKey;

  constructor(
    private http: HttpClient
  ) { }

  getCurrentWeather(location: string) {
    return this.http.get(`${environment.weatherApiUrl}/weather?q=${location}&appid=${this.apiKey}&units=metric`);
  }

}
