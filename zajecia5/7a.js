const http = require('http');
const HttpDispatcher = require('httpdispatcher');

const dispatcher = new HttpDispatcher();

const hostName = '127.0.0.1';
const portNumber = 3000;

dispatcher.onGet('/add', (req, res) => {
    const a = parseFloat(req.params.a);
    const b = parseFloat(req.params.b);
    if (!isNaN(a) && !isNaN(b)) {
        handleRequest(res, '+', a, b, a + b);
    } else {
        handleError(res);
    }
});

dispatcher.onGet('/mul', (req, res) => {
    const a = parseFloat(req.params.a);
    const b = parseFloat(req.params.b);
    if (!isNaN(a) && !isNaN(b)) {
        handleRequest(res, '*', a, b, a * b);
    } else {
        handleError(res);
    }
});

dispatcher.onGet('/div', (req, res) => {
    const a = parseFloat(req.params.a);
    const b = parseFloat(req.params.b);
    if (!isNaN(a) && !isNaN(b)) {
        handleRequest(res, '/', a, b, a / b);
    } else {
        handleError(res);
    }
});

dispatcher.onGet('/sub', (req, res) => {
    const a = parseFloat(req.params.a);
    const b = parseFloat(req.params.b);
    if (!isNaN(a) && !isNaN(b)) {
        handleRequest(res, '-', a, b, a - b);
    } else {
        handleError(res);
    }
});

dispatcher.onError((req, res) => {
    res.statusCode = 404;
    res.setHeader('Content-Type', 'text/html');
    res.end(returnHtmlResponse('Page not found', '<h1>404 - Page not found</h1>'));
})

const server = http.createServer((req, res) => {
    dispatcher.dispatch(req, res);
});

server.listen(portNumber, hostName, () => {
    console.log(`Server is up and running at http://${hostName}:${portNumber}/`);
});

function handleRequest(res, operation, a, b, result) {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    res.end(returnHtmlResponse('Wynik', `${a} ${operation} ${b} = ${result}`));
}

function handleError(res) {
    res.statusCode = 400;
    res.setHeader('Content-Type', 'text/html');
    res.end(returnHtmlResponse('Wrong argument format', '<h1>400 - Wrong argument format</h1>'))
}

function returnHtmlResponse(title, content) {
    return `<html><head><title>${title}</title></head><body>${content}</body></html>`;
}