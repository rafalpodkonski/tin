const express = require('express');
const bodyParser = require('body-parser');
const main = require('./7b/router');

const app = express();
const port = 3000;

app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', main);

app.listen(port, () => {
    console.log(`Server is up and running on port ${port}`);
});