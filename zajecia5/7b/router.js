const express = require('express');

const router = express.Router();

router.get('/hello', (req, res) => res.send('hello world'));

router.get('/form', (req, res) => res.render('./form.ejs'));

router.post('/formdata', (req, res) => {
    const formData = req.body;
    res.render('./formdata.ejs', formData);
});

router.post('/jsondata', (req, res) => {
    const jsonData = req.body;
    res.render('./jsondata.ejs', jsonData);
});

module.exports = router;