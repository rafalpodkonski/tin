import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DateBeforeValidatorDirective } from './shared/date-before-validator.directive';
import { DateAfterValidatorDirective } from './shared/date-after-validator.directive';
import { DateFromToComponent } from './date-from-to/date-from-to.component';

@NgModule({
  declarations: [
    AppComponent,
    DateBeforeValidatorDirective,
    DateAfterValidatorDirective,
    DateFromToComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
