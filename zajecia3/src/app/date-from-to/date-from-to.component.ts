import { Component } from '@angular/core';
import { FormBuilder, FormGroup, ValidatorFn, Validators } from '@angular/forms';

const dateFromToValidator: ValidatorFn = (fg: FormGroup) => {
  const from = fg.get('dateFrom');
  const to = fg.get('dateTo');
  const fromDate = new Date(from.value);
  const toDate = new Date(to.value);

  if (fromDate && toDate) {
    const toBeforeFrom = (toDate < fromDate);
    const fromAfterTo = (fromDate > toDate);
    if (toBeforeFrom || fromAfterTo) {
      return {
        beforeError: toBeforeFrom,
        afterError: fromAfterTo
      };
    }
  }
};

@Component({
  selector: 'app-date-from-to',
  templateUrl: './date-from-to.component.html',
  styleUrls: ['./date-from-to.component.sass']
})
export class DateFromToComponent {

  form: FormGroup;

  constructor(
    private fb: FormBuilder
  ) {
    this.form = this.fb.group({
      dateFrom: [null, [Validators.required]],
      dateTo: [null, [Validators.required]]
    }, {
        validator: dateFromToValidator
      });
  }

}
