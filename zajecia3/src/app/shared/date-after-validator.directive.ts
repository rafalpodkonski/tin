import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appDateAfterValidator][ngModel]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => DateAfterValidatorDirective),
    multi: true
  }]
})
export class DateAfterValidatorDirective implements Validator {

  constructor(@Attribute('appDateAfterValidator') public appDateAfterValidator: string) { }

  validate(c: AbstractControl): { [key: string]: any } {
    const currentValue = c.value;
    const secondValue = c.root.get(this.appDateAfterValidator).value;
    if (currentValue && secondValue) {
      const currentValueDate = new Date(currentValue);
      const secondValueDate = new Date(secondValue);
      const dateAfter = (currentValueDate < secondValueDate);
      const dateBefore = (currentValueDate > secondValueDate);
      if (dateAfter || dateBefore) {
        return {
          dateAfter: (currentValueDate < secondValueDate),
          dateBefore: (currentValueDate > secondValueDate)
        };
      }
    }
  }

}
