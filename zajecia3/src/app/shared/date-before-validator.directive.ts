import { Directive, forwardRef, Attribute } from '@angular/core';
import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';

@Directive({
  selector: '[appDateBeforeValidator][ngModel]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: forwardRef(() => DateBeforeValidatorDirective),
    multi: true
  }]
})
export class DateBeforeValidatorDirective implements Validator {

  constructor(@Attribute('appDateBeforeValidator') public appDateBeforeValidator: string) { }

  validate(c: AbstractControl): { [key: string]: any } {
    const currentValue = c.value;
    const secondValue = c.root.get(this.appDateBeforeValidator).value;
    if (currentValue && secondValue) {
      const currentValueDate = new Date(currentValue);
      const secondValueDate = new Date(secondValue);
      const dateBefore = (currentValueDate > secondValueDate);
      if (dateBefore) {
        return {
          dateBefore: dateBefore
        };
      }
    }
  }

}
